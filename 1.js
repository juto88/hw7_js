function filterBy(arr, dataType) {
    const filteredArr = [];  // створюємо порожній масив для зберігання відфільтрованих елементів  
    for (let i = 0; i < arr.length; i++) {// проходимось по кожному елементу масиву arr
      if (typeof arr[i] !== dataType) {
        filteredArr.push(arr[i]);// якщо тип елементу не співпадає з переданим типом даних, додаємо його до відфільтрованого масиву
      }
    } 
    return filteredArr; // повертаємо відфільтрований масив
  }
  const inputArr = ['hello', 'world', 23, '23', null];
  const filteredArr = filterBy(inputArr, 'string');
  console.log(filteredArr); // [23, null]